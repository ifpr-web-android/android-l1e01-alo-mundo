package br.edu.ifpr.android.l1ex1alomundo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class AloMundoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alo_mundo);
        TextView alo_mundo = (TextView) findViewById(R.id.alo_mundo);
        alo_mundo.setText(getString(R.string.alo_mundo));
    }
}
